﻿using Sincronize_CSA.DATA;
using Sincronize_CSA.Robot;
using System.Web;

while (true) 
{

    ///**************************************************** ASAAS *******************************////////////////////
    
    var token = new AtualizaTokenAsaas();
    var faturas = new AtualizaFaturas();
    
    Console.WriteLine("-----------BUSCANDO TOKEN ASAAS-------------");
    await token.Run();

    Console.WriteLine("-----------BUSCANDO FATURAS ASAAS-----------");
    await faturas.Run();

    Console.WriteLine("----BUSCANDO STATUS FATURAS SALVAS ASAAS----");
    await faturas.AtualizarStatusFaturas();

    Console.WriteLine("----ATUALIZANDO ADIMPLENTES ASAAS-----");
    await faturas.AtualizaAdimplentes();
    
    ///**************************************************** QGX *******************************////////////////////
 }