﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sincronize_CSA.Model
{
    public class DadosFatura
    {
        public string? id { get; set; }
        public string? dateCreated { get; set; }
        public string? customer { get; set; }
        public string? subscription { get; set; }
        public string? paymentLink { get; set; }
        public double? value { get; set; }
        public double? netValue { get; set; }
        public double? originalValue { get; set; }
        public double? interestValue { get; set; }
        public string? description { get; set; }
        public string? billingType { get; set; }
        public bool? canBePaidAfterDueDate { get; set; }
        public string? pixTransaction { get; set; }
        public string? status { get; set; }
        public string? dueDate { get; set; }
        public string? originalDueDate { get; set; }
        public DateTime? paymentDate { get; set; }
        public string? clientPaymentDate { get; set; }
        public string? installmentNumber { get; set; }
        public string? invoiceUrl { get; set; }
        public string? invoiceNumber { get; set; }
        public string? externalReference { get; set; }
        public bool? deleted { get; set; }
        public bool? anticipated { get; set; }
        public string? creditDate { get; set; }
        public string? estimatedCreditDate { get; set; }
        public string? transactionReceiptUrl { get; set; }
        public string? nossoNumero { get; set; }
        public string? bankSlipUrl { get; set; }
        public string? lastInvoiceViewedDate { get; set; }
        public string? lastBankSlipViewedDate { get; set; }
        public Discount discount { get; set; }
        public Fine fine { get; set; }
        public Interest interest { get; set; }
        public bool? postalService { get; set; }
        public List<Refund> refunds { get; set; }
    }

    public partial class Refund
    {
        public int id { get; set; }
        public DateTime? DateCreated { get; set; }
        public string? Status { get; set; }
        public double? Value { get; set; }
        public string? Description { get; set; }
        public string?TransactionReceiptUrl { get; set; }
    }

    public class Discount
    {
        public int? id { get; set; }
        public double? value { get; set; }
        public string? limitDate { get; set; }
        public int? dueDateLimitDays { get; set; }
        public string? type { get; set; }
    }

    public class Fine
    {
        public int? id { get; set; }
        public double? value { get; set; }
        public string? type { get; set; }
    }

    public class Interest
    {
        public int? id { get; set; }
        public double? value { get; set; }
        public string? type { get; set; }
    }

    public class Faturas
    {
        public int? id { get; set; }
        public bool? hasMore { get; set; }
        public int? totalCount { get; set; }
        public int? limit { get; set; }
        public int? offset { get; set; }
        public List<DadosFatura> data { get; set; }
    }


}
