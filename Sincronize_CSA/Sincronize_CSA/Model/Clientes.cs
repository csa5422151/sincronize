﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sincronize_CSA.Model
{
    public class ClientesDados
    {
        public string? id { get; set; }
        public string? dateCreated { get; set; }
        public string? name { get; set; }
        public string? email { get; set; }
        public object? company { get; set; }
        public object? phone { get; set; }
        public string? mobilePhone { get; set; }
        public string? address { get; set; }
        public string? addressNumber { get; set; }
        public string? complement { get; set; }
        public string? province { get; set; }
        public string? postalCode { get; set; }
        public string? cpfCnpj { get; set; }
        public string? personType { get; set; }
        public bool deleted { get; set; }
        public object? additionalEmails { get; set; }
        public object? externalReference { get; set; }
        public bool notificationDisabled { get; set; }
        public object? observations { get; set; }
        public object? municipalInscription { get; set; }
        public object? stateInscription { get; set; }
        public bool canDelete { get; set; }
        public object? cannotBeDeletedReason { get; set; }
        public bool canEdit { get; set; }
        public object? cannotEditReason { get; set; }
        public bool foreignCustomer { get; set; }
        public int? city { get; set; }
        public string? state { get; set; }
        public string? country { get; set; }            
    }
    public class Clientes
    {
        public bool hasMore { get; set; }
        public int totalCount { get; set; }
        public int limit { get; set; }
        public int offset { get; set; }
        public List<ClientesDados> data { get; set; }
    }
}
