﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sincronize_CSA.Model
{
    public enum EFuncionalidades
    {
        AtualizaTokenAsaas = 1,
        AtualizaFaturas = 2,
        AtualizaStatusFaturas = 3,
        AtualizaAdimplentesFaturas = 4,

        AtualizaTokenAsaasQGX = 5,
        AtualizaFaturasQGX = 6,
        AtualizaStatusFaturasQGX = 7,
        AtualizaAdimplentesFaturasQGX = 8,
    }
}
