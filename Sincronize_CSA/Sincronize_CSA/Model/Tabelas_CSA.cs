﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sincronize_CSA.Model
{
	public class accepts
	{
		public int? id { get; set; }
		public string? value { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }

		public accepts(int? id_, string? value_, DateTime? created_at_, DateTime? updated_at_)
		{
			this.id = id_;
			this.value = value_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
		}

        public accepts()
        {

        }
	}

	public class admins
	{
		public int? id { get; set; }
		public string? name { get; set; }
		public string? cpf { get; set; }
		public string? rg { get; set; }
		public string? orgao_expeditor { get; set; }
		public string? data_nascimento { get; set; }
		public string? estado_civil { get; set; }
		public string? telefone_residencial { get; set; }
		public string? telefone_comercial { get; set; }
		public string? celular { get; set; }
		public string? celular_1 { get; set; }
		public string? cep { get; set; }
		public string? endereco { get; set; }
		public string? numero { get; set; }
		public string? cidade { get; set; }
		public string? bairro { get; set; }
		public string? estado { get; set; }
		public string? complemento { get; set; }
		public string? email { get; set; }
		public string? password { get; set; }
		public byte is_super { get; set; }
		public string? remember_token { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public int? id_admin { get; set; }
		public int? is_atendimento { get; set; }

		public admins(int? id_, string? name_, string? cpf_, string? rg_, string? orgao_expeditor_, string? data_nascimento_, string? estado_civil_, string? telefone_residencial_, string? telefone_comercial_, string? celular_, string? celular_1_, string? cep_, string? endereco_, string? numero_, string? cidade_, string? bairro_, string? estado_, string? complemento_, string? email_, string? password_, byte is_super_, string? remember_token_, DateTime? created_at_, DateTime? updated_at_, int? id_admin_, int? is_atendimento_)
		{
			this.id = id_;
			this.name = name_;
			this.cpf = cpf_;
			this.rg = rg_;
			this.orgao_expeditor = orgao_expeditor_;
			this.data_nascimento = data_nascimento_;
			this.estado_civil = estado_civil_;
			this.telefone_residencial = telefone_residencial_;
			this.telefone_comercial = telefone_comercial_;
			this.celular = celular_;
			this.celular_1 = celular_1_;
			this.cep = cep_;
			this.endereco = endereco_;
			this.numero = numero_;
			this.cidade = cidade_;
			this.bairro = bairro_;
			this.estado = estado_;
			this.complemento = complemento_;
			this.email = email_;
			this.password = password_;
			this.is_super = is_super_;
			this.remember_token = remember_token_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
			this.id_admin = id_admin_;
			this.is_atendimento = is_atendimento_;
		}

        public admins()
        {

        }
	}

	public class AgendamentoApp
	{
		public int? id { get; set; }
		public string? id_cliente { get; set; }
		public string? procedimento { get; set; }
		public string? cidade { get; set; }

		public AgendamentoApp(int? id_, string? id_cliente_, string? procedimento_, string? cidade_)
		{
			this.id = id_;
			this.id_cliente = id_cliente_;
			this.procedimento = procedimento_;
			this.cidade = cidade_;
		}

        public AgendamentoApp()
        {

        }
	}

	public class agendamento_apps
	{
		public int? id { get; set; }
		public string? id_cliente { get; set; }
		public string? cidade { get; set; }
		public string? procedimento { get; set; }
		public DateTime? updated_at { get; set; }
		public DateTime? created_at { get; set; }

		public agendamento_apps(int? id_, string? id_cliente_, string? cidade_, string? procedimento_, DateTime? updated_at_, DateTime? created_at_)
		{
			this.id = id_;
			this.id_cliente = id_cliente_;
			this.cidade = cidade_;
			this.procedimento = procedimento_;
			this.updated_at = updated_at_;
			this.created_at = created_at_;
		}

        public agendamento_apps()
        {

        }
	}

	public class agregados
	{
		public int? id { get; set; }
		public int? id_clientes { get; set; }
		public string? nome { get; set; }
		public string? cpf { get; set; }
		public string? data_nascimento { get; set; }
		public string? valor { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string? matricula { get; set; }
		public string? numero_carteirinha { get; set; }
		public string? senha_app { get; set; }

		public agregados(int? id_, int? id_clientes_, string? nome_, string? cpf_, string? data_nascimento_, string? valor_, DateTime? created_at_, DateTime? updated_at_, string? matricula_, string? numero_carteirinha_, string? senha_app_)
		{
			this.id = id_;
			this.id_clientes = id_clientes_;
			this.nome = nome_;
			this.cpf = cpf_;
			this.data_nascimento = data_nascimento_;
			this.valor = valor_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
			this.matricula = matricula_;
			this.numero_carteirinha = numero_carteirinha_;
			this.senha_app = senha_app_;
		}

        public agregados()
        {

        }
	}

	public class assinaturas_canceladas
	{
		public int? id { get; set; }
		public string? id_assinatura { get; set; }
		public string? status { get; set; }

		public assinaturas_canceladas(int? id_, string? id_assinatura_, string? status_)
		{
			this.id = id_;
			this.id_assinatura = id_assinatura_;
			this.status = status_;
		}

        public assinaturas_canceladas()
        {

        }
	}

	public class back_offices
	{
		public int? id { get; set; }
		public string? name { get; set; }
		public string? cpf { get; set; }
		public string? rg { get; set; }
		public string? orgao_expeditor { get; set; }
		public string? data_nascimento { get; set; }
		public string? estado_civil { get; set; }
		public string? telefone_residencial { get; set; }
		public string? telefone_comercial { get; set; }
		public string? celular { get; set; }
		public string? celular_1 { get; set; }
		public string? cep { get; set; }
		public string? endereco { get; set; }
		public string? numero { get; set; }
		public string? cidade { get; set; }
		public string? bairro { get; set; }
		public string? estado { get; set; }
		public string? complemento { get; set; }
		public string? email { get; set; }
		public string? password { get; set; }
		public byte is_super { get; set; }
		public string? remember_token { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }

		public back_offices(int? id_, string? name_, string? cpf_, string? rg_, string? orgao_expeditor_, string? data_nascimento_, string? estado_civil_, string? telefone_residencial_, string? telefone_comercial_, string? celular_, string? celular_1_, string? cep_, string? endereco_, string? numero_, string? cidade_, string? bairro_, string? estado_, string? complemento_, string? email_, string? password_, byte is_super_, string? remember_token_, DateTime? created_at_, DateTime? updated_at_)
		{
			this.id = id_;
			this.name = name_;
			this.cpf = cpf_;
			this.rg = rg_;
			this.orgao_expeditor = orgao_expeditor_;
			this.data_nascimento = data_nascimento_;
			this.estado_civil = estado_civil_;
			this.telefone_residencial = telefone_residencial_;
			this.telefone_comercial = telefone_comercial_;
			this.celular = celular_;
			this.celular_1 = celular_1_;
			this.cep = cep_;
			this.endereco = endereco_;
			this.numero = numero_;
			this.cidade = cidade_;
			this.bairro = bairro_;
			this.estado = estado_;
			this.complemento = complemento_;
			this.email = email_;
			this.password = password_;
			this.is_super = is_super_;
			this.remember_token = remember_token_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
		}

        public back_offices()
        {

        }
	}

	public class boleto_carnes
	{
		public int? id { get; set; }
		public string? dia_vencimento { get; set; }
		public string? tipo { get; set; }
		public int? id_cliente { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }

		public boleto_carnes(int? id_, string? dia_vencimento_, string? tipo_, int? id_cliente_, DateTime? created_at_, DateTime? updated_at_)
		{
			this.id = id_;
			this.dia_vencimento = dia_vencimento_;
			this.tipo = tipo_;
			this.id_cliente = id_cliente_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
		}

        public boleto_carnes()
        {

        }
	}

	public class canceladas
	{
		public int? id { get; set; }
		public string? id_cobranca { get; set; }
		public string? status { get; set; }

		public canceladas(int? id_, string? id_cobranca_, string? status_)
		{
			this.id = id_;
			this.id_cobranca = id_cobranca_;
			this.status = status_;
		}

        public canceladas()
        {

        }
	}

	public class cartao_de_creditos
	{
		public int? id { get; set; }
		public string? nome_cartao { get; set; }
		public string? numero_cartao { get; set; }
		public string? validade { get; set; }
		public string? cvv { get; set; }
		public int? id_cliente { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string? dia_vencimento { get; set; }

		public cartao_de_creditos(int? id_, string? nome_cartao_, string? numero_cartao_, string? validade_, string? cvv_, int? id_cliente_, DateTime? created_at_, DateTime? updated_at_, string? dia_vencimento_)
		{
			this.id = id_;
			this.nome_cartao = nome_cartao_;
			this.numero_cartao = numero_cartao_;
			this.validade = validade_;
			this.cvv = cvv_;
			this.id_cliente = id_cliente_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
			this.dia_vencimento = dia_vencimento_;
		}
        public cartao_de_creditos()
        {

        }
	}

	public class categoria_material_apoios
	{
		public int? id { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }

		public categoria_material_apoios(int? id_, DateTime? created_at_, DateTime? updated_at_)
		{
			this.id = id_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
		}

        public categoria_material_apoios()
        {

        }
	}

	public class cidades
	{
		public int? id { get; set; }
		public string? nome { get; set; }
		public string? uf { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }

		public cidades(int? id_, string? nome_, string? uf_, DateTime? created_at_, DateTime? updated_at_)
		{
			this.id = id_;
			this.nome = nome_;
			this.uf = uf_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
		}

        public cidades()
        {

        }
	}

	public class cidade_parceiros
	{
		public int? id { get; set; }
		public string? cidade { get; set; }
		public int? id_parceiro { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }

		public cidade_parceiros(int? id_, string? cidade_, int? id_parceiro_, DateTime? created_at_, DateTime? updated_at_)
		{
			this.id = id_;
			this.cidade = cidade_;
			this.id_parceiro = id_parceiro_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
		}

        public cidade_parceiros()
        {

        }
	}

	public class clientes
	{
		public int? id { get; set; }
		public string? nome_completo { get; set; }
		public string? cpf { get; set; }
		public string? rg { get; set; }
		public string? orgao_expeditor { get; set; }
		public string? data_nascimento { get; set; }
		public string? estado_civil { get; set; }
		public string? profissao { get; set; }
		public string? renda { get; set; }
		public string? email { get; set; }
		public string? telefone_residencial { get; set; }
		public string? telefone_comercial { get; set; }
		public string? celular { get; set; }
		public string? celular_1 { get; set; }
		public string? cep { get; set; }
		public string? endereco { get; set; }
		public string? complemento { get; set; }
		public string? numero { get; set; }
		public string? cidade { get; set; }
		public string? bairro { get; set; }
		public string? estado { get; set; }
		public string? forma_pagamento { get; set; }
		public int? id_pagamento { get; set; }
		public int? id_produto { get; set; }
		public string? periodicidade { get; set; }
		public int? uploader_count { get; set; }
		public int? id_vendedor { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string? matricula { get; set; }
		public string? numero_carteirinha { get; set; }
		public string? carteirinha_emitida { get; set; }
		public string? link_gravacao { get; set; }
		public string? indicacao { get; set; }
		public string? token_asaas { get; set; }
		public int? flg_cobranca { get; set; }
		public int? adimplente { get; set; }
		public int? adesao_paga { get; set; }
		public string? historico { get; set; }
		public string? token_assinatura { get; set; }
		public string? parcela_paga { get; set; }
		public string? parcela_atrasada { get; set; }
		public string? total_parcela { get; set; }
		public string? cidade_relatorio { get; set; }
		public string? senha_app { get; set; }
		public string? valor_atrasado { get; set; }
		public string? sexo { get; set; }
		public string? cliente_pj { get; set; }
		public string? responsavelFinanceiro { get; set; }
		public string? amigo_indica { get; set; }
		public int? cidade_referencia { get; set; }
		public DateTime? inicio_contrato { get; set; }
		public int? id_parceiro { get; set; }
		public DateTime? fim_contrato { get; set; }

		public clientes(int? id_, string? nome_completo_, string? cpf_, string? rg_, string? orgao_expeditor_, string? data_nascimento_, string? estado_civil_, string? profissao_, string? renda_, string? email_, string? telefone_residencial_, string? telefone_comercial_, string? celular_, string? celular_1_, string? cep_, string? endereco_, string? complemento_, string? numero_, string? cidade_, string? bairro_, string? estado_, string? forma_pagamento_, int? id_pagamento_, int? id_produto_, string? periodicidade_, int? uploader_count_, int? id_vendedor_, DateTime? created_at_, DateTime? updated_at_, string? matricula_, string? numero_carteirinha_, string? carteirinha_emitida_, string? link_gravacao_, string? indicacao_, string? token_asaas_, int? flg_cobranca_, int? adimplente_, int? adesao_paga_, string? historico_, string? token_assinatura_, string? parcela_paga_, string? parcela_atrasada_, string? total_parcela_, string? cidade_relatorio_, string? senha_app_, string? valor_atrasado_, string? sexo_, string? cliente_pj_, string? responsavelFinanceiro_, string? amigo_indica_, int? cidade_referencia_, DateTime? inicio_contrato_, int? id_parceiro_, DateTime? fim_contrato_)
		{
			this.id = id_;
			this.nome_completo = nome_completo_;
			this.cpf = cpf_;
			this.rg = rg_;
			this.orgao_expeditor = orgao_expeditor_;
			this.data_nascimento = data_nascimento_;
			this.estado_civil = estado_civil_;
			this.profissao = profissao_;
			this.renda = renda_;
			this.email = email_;
			this.telefone_residencial = telefone_residencial_;
			this.telefone_comercial = telefone_comercial_;
			this.celular = celular_;
			this.celular_1 = celular_1_;
			this.cep = cep_;
			this.endereco = endereco_;
			this.complemento = complemento_;
			this.numero = numero_;
			this.cidade = cidade_;
			this.bairro = bairro_;
			this.estado = estado_;
			this.forma_pagamento = forma_pagamento_;
			this.id_pagamento = id_pagamento_;
			this.id_produto = id_produto_;
			this.periodicidade = periodicidade_;
			this.uploader_count = uploader_count_;
			this.id_vendedor = id_vendedor_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
			this.matricula = matricula_;
			this.numero_carteirinha = numero_carteirinha_;
			this.carteirinha_emitida = carteirinha_emitida_;
			this.link_gravacao = link_gravacao_;
			this.indicacao = indicacao_;
			this.token_asaas = token_asaas_;
			this.flg_cobranca = flg_cobranca_;
			this.adimplente = adimplente_;
			this.adesao_paga = adesao_paga_;
			this.historico = historico_;
			this.token_assinatura = token_assinatura_;
			this.parcela_paga = parcela_paga_;
			this.parcela_atrasada = parcela_atrasada_;
			this.total_parcela = total_parcela_;
			this.cidade_relatorio = cidade_relatorio_;
			this.senha_app = senha_app_;
			this.valor_atrasado = valor_atrasado_;
			this.sexo = sexo_;
			this.cliente_pj = cliente_pj_;
			this.responsavelFinanceiro = responsavelFinanceiro_;
			this.amigo_indica = amigo_indica_;
			this.cidade_referencia = cidade_referencia_;
			this.inicio_contrato = inicio_contrato_;
			this.id_parceiro = id_parceiro_;
			this.fim_contrato = fim_contrato_;
		}

        public clientes()
        {

        }
	}

	public class clientes_pjs
	{
		public int? id { get; set; }
		public string? id_vendedor { get; set; }
		public string? nome_empresa { get; set; }
		public string? cnpj { get; set; }
		public string? inscricao { get; set; }
		public string? telefone_comercial { get; set; }
		public string? cep { get; set; }
		public string? endereco { get; set; }
		public string? complemento { get; set; }
		public string? numero { get; set; }
		public string? nome_responsavel { get; set; }
		public string? cpf { get; set; }
		public string? celular { get; set; }
		public string? qtd_colaboradores { get; set; }
		public string? registrados { get; set; }
		public string? acima_70 { get; set; }
		public string? gfpi { get; set; }
		public string? prestador { get; set; }
		public string? total_mensal { get; set; }
		public string? id_produto { get; set; }
		public string? status { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public int? supervisor { get; set; }
		public string? medicinaDoTrabalho { get; set; }

		public clientes_pjs(int? id_, string? id_vendedor_, string? nome_empresa_, string? cnpj_, string? inscricao_, string? telefone_comercial_, string? cep_, string? endereco_, string? complemento_, string? numero_, string? nome_responsavel_, string? cpf_, string? celular_, string? qtd_colaboradores_, string? registrados_, string? acima_70_, string? gfpi_, string? prestador_, string? total_mensal_, string? id_produto_, string? status_, DateTime? created_at_, DateTime? updated_at_, int? supervisor_, string? medicinaDoTrabalho_)
		{
			this.id = id_;
			this.id_vendedor = id_vendedor_;
			this.nome_empresa = nome_empresa_;
			this.cnpj = cnpj_;
			this.inscricao = inscricao_;
			this.telefone_comercial = telefone_comercial_;
			this.cep = cep_;
			this.endereco = endereco_;
			this.complemento = complemento_;
			this.numero = numero_;
			this.nome_responsavel = nome_responsavel_;
			this.cpf = cpf_;
			this.celular = celular_;
			this.qtd_colaboradores = qtd_colaboradores_;
			this.registrados = registrados_;
			this.acima_70 = acima_70_;
			this.gfpi = gfpi_;
			this.prestador = prestador_;
			this.total_mensal = total_mensal_;
			this.id_produto = id_produto_;
			this.status = status_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
			this.supervisor = supervisor_;
			this.medicinaDoTrabalho = medicinaDoTrabalho_;
		}

        public clientes_pjs()
        {

        }
	}

	public class comissoes_vendas
	{
		public int? id { get; set; }
		public string? id_clientes { get; set; }
		public string? id_supervisor { get; set; }
		public string? id_vendedor { get; set; }
		public string? id_venda_gerada { get; set; }
		public string? status { get; set; }
		public string? pago { get; set; }
		public string? pago_vendedor { get; set; }
		public string? pago_supervisor { get; set; }
		public string? valor { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string? parcela { get; set; }

		public comissoes_vendas(int? id_, string? id_clientes_, string? id_supervisor_, string? id_vendedor_, string? id_venda_gerada_, string? status_, string? pago_, string? pago_vendedor_, string? pago_supervisor_, string? valor_, DateTime? created_at_, DateTime? updated_at_, string? parcela_)
		{
			this.id = id_;
			this.id_clientes = id_clientes_;
			this.id_supervisor = id_supervisor_;
			this.id_vendedor = id_vendedor_;
			this.id_venda_gerada = id_venda_gerada_;
			this.status = status_;
			this.pago = pago_;
			this.pago_vendedor = pago_vendedor_;
			this.pago_supervisor = pago_supervisor_;
			this.valor = valor_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
			this.parcela = parcela_;
		}

        public comissoes_vendas()
        {

        }
	}

	public class consultas
	{
		public int? id { get; set; }
		public int? id_cliente { get; set; }
		public string? especialidade { get; set; }
		public string? unidade { get; set; }
		public string? token { get; set; }
		public string? status { get; set; }
		public DateTime? updated_at { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? horario { get; set; }
		public string? procedimentos { get; set; }
		public int? id_procedimento { get; set; }
		public int? id_parceiro { get; set; }
		public string? cidade { get; set; }
		public string? valor_total { get; set; }
		public string? observacoes { get; set; }

		public consultas(int? id_, int? id_cliente_, string? especialidade_, string? unidade_, string? token_, string? status_, DateTime? updated_at_, DateTime? created_at_, DateTime? horario_, string? procedimentos_, int? id_procedimento_, int? id_parceiro_, string? cidade_, string? valor_total_, string? observacoes_)
		{
			this.id = id_;
			this.id_cliente = id_cliente_;
			this.especialidade = especialidade_;
			this.unidade = unidade_;
			this.token = token_;
			this.status = status_;
			this.updated_at = updated_at_;
			this.created_at = created_at_;
			this.horario = horario_;
			this.procedimentos = procedimentos_;
			this.id_procedimento = id_procedimento_;
			this.id_parceiro = id_parceiro_;
			this.cidade = cidade_;
			this.valor_total = valor_total_;
			this.observacoes = observacoes_;
		}

        public consultas()
        {

        }
	}

	public class contratos
	{
		public int? id { get; set; }
		public int? id_cliente { get; set; }
		public int? id_vendedor { get; set; }
		public int? id_produto { get; set; }
		public int? id_fornecedor { get; set; }
		public string? forma_pagamento { get; set; }
		public int? id_pagamento { get; set; }
		public int? id_supervisor { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }

		public contratos(int? id_, int? id_cliente_, int? id_vendedor_, int? id_produto_, int? id_fornecedor_, string? forma_pagamento_, int? id_pagamento_, int? id_supervisor_, DateTime? created_at_, DateTime? updated_at_)
		{
			this.id = id_;
			this.id_cliente = id_cliente_;
			this.id_vendedor = id_vendedor_;
			this.id_produto = id_produto_;
			this.id_fornecedor = id_fornecedor_;
			this.forma_pagamento = forma_pagamento_;
			this.id_pagamento = id_pagamento_;
			this.id_supervisor = id_supervisor_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
		}

        public contratos()
        {

        }
	}

	public class debito_em_contas
	{
		public int? id { get; set; }
		public string? banco { get; set; }
		public string? numero_conta { get; set; }
		public string? agencia { get; set; }
		public string? operacao { get; set; }
		public string? dia_debito { get; set; }
		public int? id_cliente { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string? dia_vencimento { get; set; }

		public debito_em_contas(int? id_, string? banco_, string? numero_conta_, string? agencia_, string? operacao_, string? dia_debito_, int? id_cliente_, DateTime? created_at_, DateTime? updated_at_, string? dia_vencimento_)
		{
			this.id = id_;
			this.banco = banco_;
			this.numero_conta = numero_conta_;
			this.agencia = agencia_;
			this.operacao = operacao_;
			this.dia_debito = dia_debito_;
			this.id_cliente = id_cliente_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
			this.dia_vencimento = dia_vencimento_;
		}

        public debito_em_contas()
        {

        }
	}

	public class dependentes
	{
		public int? id { get; set; }
		public int? id_clientes { get; set; }
		public string? nome { get; set; }
		public string? cpf { get; set; }
		public string? data_nascimento { get; set; }
		public string? parentesco { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string? matricula { get; set; }
		public string? numero_carteirinha { get; set; }
		public string? senha_app { get; set; }

		public dependentes(int? id_, int? id_clientes_, string? nome_, string? cpf_, string? data_nascimento_, string? parentesco_, DateTime? created_at_, DateTime? updated_at_, string? matricula_, string? numero_carteirinha_, string? senha_app_)
		{
			this.id = id_;
			this.id_clientes = id_clientes_;
			this.nome = nome_;
			this.cpf = cpf_;
			this.data_nascimento = data_nascimento_;
			this.parentesco = parentesco_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
			this.matricula = matricula_;
			this.numero_carteirinha = numero_carteirinha_;
			this.senha_app = senha_app_;
		}

        public dependentes()
        {

        }
	}

	public class estornos
	{
		public int? id { get; set; }
		public string? id_cobranca { get; set; }
		public string? status { get; set; }

		public estornos(int? id_, string? id_cobranca_, string? status_)
		{
			this.id = id_;
			this.id_cobranca = id_cobranca_;
			this.status = status_;
		}

        public estornos()
        {

        }
	}

	public class faturas_historicos
	{
		public int? id { get; set; }
		public string? status { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }

		public faturas_historicos(int? id_, string? status_, DateTime? created_at_, DateTime? updated_at_)
		{
			this.id = id_;
			this.status = status_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
		}

        public faturas_historicos()
        {

        }
	}

	public class fatura_clientes
	{
		public int? id { get; set; }
		public int? id_contrato { get; set; }
		public int? id_cliente { get; set; }
		public int? id_vendedor { get; set; }
		public int? id_supervisor { get; set; }
		public string? status { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string? parcela { get; set; }
		public byte[] data_vencimento { get; set; }
		public string? fatura_cliente { get; set; }
		public string? valor { get; set; }
		public string? observacoes { get; set; }
		public byte[] data_pagamento { get; set; }

		public fatura_clientes(int? id_, int? id_contrato_, int? id_cliente_, int? id_vendedor_, int? id_supervisor_, string? status_, DateTime? created_at_, DateTime? updated_at_, string? parcela_, byte[] data_vencimento_, string? fatura_cliente_, string? valor_, string? observacoes_, byte[] data_pagamento_)
		{
			this.id = id_;
			this.id_contrato = id_contrato_;
			this.id_cliente = id_cliente_;
			this.id_vendedor = id_vendedor_;
			this.id_supervisor = id_supervisor_;
			this.status = status_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
			this.parcela = parcela_;
			this.data_vencimento = data_vencimento_;
			this.fatura_cliente = fatura_cliente_;
			this.valor = valor_;
			this.observacoes = observacoes_;
			this.data_pagamento = data_pagamento_;
		}

        public fatura_clientes()
        {

        }
	}

	public class fatura_supervisores
	{
		public int? id { get; set; }
		public int? id_contrato { get; set; }
		public int? id_cliente { get; set; }
		public int? id_vendedor { get; set; }
		public int? id_supervisor { get; set; }
		public int? status { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string? parcela { get; set; }

		public fatura_supervisores(int? id_, int? id_contrato_, int? id_cliente_, int? id_vendedor_, int? id_supervisor_, int? status_, DateTime? created_at_, DateTime? updated_at_, string? parcela_)
		{
			this.id = id_;
			this.id_contrato = id_contrato_;
			this.id_cliente = id_cliente_;
			this.id_vendedor = id_vendedor_;
			this.id_supervisor = id_supervisor_;
			this.status = status_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
			this.parcela = parcela_;
		}

        public fatura_supervisores()
        {

        }
	}

	public class fatura_vendedores
	{
		public int? id { get; set; }
		public int? id_contrato { get; set; }
		public int? id_cliente { get; set; }
		public int? id_vendedor { get; set; }
		public int? id_supervisor { get; set; }
		public int? status { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string? parcela { get; set; }

		public fatura_vendedores(int? id_, int? id_contrato_, int? id_cliente_, int? id_vendedor_, int? id_supervisor_, int? status_, DateTime? created_at_, DateTime? updated_at_, string? parcela_)
		{
			this.id = id_;
			this.id_contrato = id_contrato_;
			this.id_cliente = id_cliente_;
			this.id_vendedor = id_vendedor_;
			this.id_supervisor = id_supervisor_;
			this.status = status_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
			this.parcela = parcela_;
		}

        public fatura_vendedores()
        {

        }
	}

	public class fornecedores
	{
		public int? id { get; set; }
		public string? nome { get; set; }
		public string? nome_contato { get; set; }
		public string? email_contato { get; set; }
		public string? telefone_contato { get; set; }
		public string? qual_seguimento { get; set; }
		public int? id_admin { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }

		public fornecedores(int? id_, string? nome_, string? nome_contato_, string? email_contato_, string? telefone_contato_, string? qual_seguimento_, int? id_admin_, DateTime? created_at_, DateTime? updated_at_)
		{
			this.id = id_;
			this.nome = nome_;
			this.nome_contato = nome_contato_;
			this.email_contato = email_contato_;
			this.telefone_contato = telefone_contato_;
			this.qual_seguimento = qual_seguimento_;
			this.id_admin = id_admin_;
			this.created_at = created_at_;
			this.updated_at = updated_at_;
		}

        public fornecedores()
        {

        }
	}

	public class interessados
	{
		public int? id { get; set; }
		public string? nome { get; set; }
		public string? telefone { get; set; }
		public string? observacoes { get; set; }
		public DateTime? updated_at { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? data_atendimento { get; set; }
		public string? link_perfil_redes { get; set; }
		public string? cidade { get; set; }
		public string? convertido { get; set; }
		public string? motivo_nao_convertido { get; set; }
		public string? canal { get; set; }
		public string? atendente { get; set; }
		public string? vendedor_id { get; set; }
		public string? historico { get; set; }
		public string? status { get; set; }

		public interessados(int? id_, string? nome_, string? telefone_, string? observacoes_, DateTime? updated_at_, DateTime? created_at_, DateTime? data_atendimento_, string? link_perfil_redes_, string? cidade_, string? convertido_, string? motivo_nao_convertido_, string? canal_, string? atendente_, string? vendedor_id_, string? historico_, string? status_)
		{
			this.id = id_;
			this.nome = nome_;
			this.telefone = telefone_;
			this.observacoes = observacoes_;
			this.updated_at = updated_at_;
			this.created_at = created_at_;
			this.data_atendimento = data_atendimento_;
			this.link_perfil_redes = link_perfil_redes_;
			this.cidade = cidade_;
			this.convertido = convertido_;
			this.motivo_nao_convertido = motivo_nao_convertido_;
			this.canal = canal_;
			this.atendente = atendente_;
			this.vendedor_id = vendedor_id_;
			this.historico = historico_;
			this.status = status_;
		}

        public interessados()
        {

        }
	}

	public class material_apoios
	{
		public int? id { get; set; }
		public string? titulo { get; set; }
		public int? id_categoria { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }

        public material_apoios()
        {

        }
	}

	public class migrations
	{
		public int? id { get; set; }
		public string? migration { get; set; }
		public int? batch { get; set; }

        public migrations()
        {

        }
	}

	public class parceiros
	{
		public int? id { get; set; }
		public string? razao_social { get; set; }
		public string? cpf { get; set; }
		public string? cnpj { get; set; }
		public string? email { get; set; }
		public string? senha { get; set; }
		public string? perfil { get; set; }
		public string? telefone_comercial { get; set; }
		public string? cep { get; set; }
		public string? endereco { get; set; }
		public string? complemento { get; set; }
		public string? numero { get; set; }
		public string? cidade { get; set; }
		public string? bairro { get; set; }
		public string? estado { get; set; }
		public string? faixa_cep_inicio { get; set; }
		public string? faixa_cep_fim { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string? celular { get; set; }
		public string? celular_1 { get; set; }
		public string? responsavel_empresa { get; set; }
		public string? password { get; set; }
		public string? name { get; set; }
		public string? remember_token { get; set; }
		public string? bairros_atendidos { get; set; }
		public string? participacao { get; set; }

        public parceiros()
        {

        }
	}

	public class parceiros_precos
	{
		public int? id { get; set; }
		public string? id_parceiro { get; set; }
		public string? descricao { get; set; }
		public string? custo { get; set; }
		public DateTime? updated_at { get; set; }
		public DateTime? created_at { get; set; }
	}

	public class parcerias_corporativas
	{
		public int? id { get; set; }
		public int? cliente_id { get; set; }
		public string? dia_vencimento { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
	}

	//public class password_resets
	//{
	//	public string? email { get; set; }
	//	public string? token { get; set; }
	//	public DateTime? created_at { get; set; }
	//}

	public class pixes
	{
		public int? id { get; set; }
		public string? descricao { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public int? id_cliente { get; set; }
		public string? dia_vencimento { get; set; }
	}

	public class procedimentos
	{
		public int? id { get; set; }
		public string? nome { get; set; }
		public string? valor { get; set; }
		public string? unidade { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? update_at { get; set; }
		public int? id_parceiro { get; set; }
		public string? tipo { get; set; }
		public string? localidade { get; set; }
	}

	public class produtos
	{
		public int? id { get; set; }
		public string? nome { get; set; }
		public int? id_fornecedor { get; set; }
		public int? id_admin { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string? disponivel { get; set; }
        public double? valor { get; set; }
    }

	public class templates_contratos
	{
		public int? id { get; set; }
		public string? body { get; set; }
	}

	public class uploads
	{
		public int? id { get; set; }
		public string? nome { get; set; }
		public string? extension { get; set; }
		public string? path { get; set; }
		public int? id_cliente { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
	}

	public class users
	{
		public int? id { get; set; }
		public string? name { get; set; }
		public string? cpf { get; set; }
		public string? cnpj { get; set; }
		public string? pf { get; set; }
		public string? rg { get; set; }
		public string? orgao_expeditor { get; set; }
		public string? data_nascimento { get; set; }
		public string? estado_civil { get; set; }
		public string? telefone_residencial { get; set; }
		public string? telefone_comercial { get; set; }
		public string? celular { get; set; }
		public string? celular_1 { get; set; }
		public string? cep { get; set; }
		public string? endereco { get; set; }
		public string? numero { get; set; }
		public string? cidade { get; set; }
		public string? bairro { get; set; }
		public string? estado { get; set; }
		public string? complemento { get; set; }
		public string? taxa_adesao { get; set; }
		public string? remuneracao_vitalicia { get; set; }
		public string? valor_adesao { get; set; }
		public string? valor_vitalicio { get; set; }
		public string? despesa_operacional { get; set; }
		public string? email { get; set; }
		public string? password { get; set; }
		public int? id_supervisor { get; set; }
		public byte bloqueado { get; set; }
		public string? remember_token { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string? cadastra_cliente { get; set; }
		public string? nome_empresa { get; set; }
		public string? inscricao { get; set; }
	}

	public class venda_geradas
	{
		public int? id { get; set; }
		public int? id_cliente { get; set; }
		public int? id_vendedor { get; set; }
		public int? id_supervisor { get; set; }
		public short? status { get; set; }
		public short? rejeitado { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
	}

	public class writers
	{
		public int? id { get; set; }
		public string? name { get; set; }
		public string? cpf { get; set; }
		public string? rg { get; set; }
		public string? orgao_expeditor { get; set; }
		public string? data_nascimento { get; set; }
		public string? estado_civil { get; set; }
		public string? telefone_residencial { get; set; }
		public string? telefone_comercial { get; set; }
		public string? celular { get; set; }
		public string? celular_1 { get; set; }
		public string? cep { get; set; }
		public string? endereco { get; set; }
		public string? numero { get; set; }
		public string? cidade { get; set; }
		public string? bairro { get; set; }
		public string? estado { get; set; }
		public string? complemento { get; set; }
		public string? taxa_adesao { get; set; }
		public string? remuneracao_vitalicia { get; set; }
		public string? valor_adesao { get; set; }
		public string? valor_vitalicio { get; set; }
		public string? despesa_operacional { get; set; }
		public string? email { get; set; }
		public string? password { get; set; }
		public byte is_editor { get; set; }
		public int? id_admin { get; set; }
		public byte bloqueado { get; set; }
		public string? remember_token { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string? is_plataforma { get; set; }
		public string? nome_empresa { get; set; }
		public string? cnpj { get; set; }
		public string? inscricao { get; set; }
	}
}
