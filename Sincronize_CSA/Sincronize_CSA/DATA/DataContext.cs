﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Sincronize_CSA.Model;

namespace Sincronize_CSA.DATA
{


    public class ApplicationDbContext : DbContext
    {
        protected readonly IConfiguration Configuration;

        public ApplicationDbContext()
        {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // connect to mysql with connection string from app settings
            var connectionString = "server=162.241.203.180;user=sempr524_sincronize;database=sempr524_producao;password=S3mPr3@migg0";
            options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
        }

		public DbSet<DadosFatura> DadosFatura { get; set; }
		public DbSet<Discount> Discount { get; set; }
		public DbSet<Fine> Fine { get; set; }
		public DbSet<Interest> Interest { get; set; }
		public DbSet<Faturas> Faturas { get; set; }
		public DbSet<Refund> Refund { get; set; }		
		public DbSet<accepts> accepts { get; set; }
		public DbSet<admins> admins { get; set; }
		public DbSet<AgendamentoApp> AgendamentoApp { get; set; }
		public DbSet<agendamento_apps> agendamento_apps { get; set; }
		public DbSet<agregados> agregados { get; set; }
		public DbSet<assinaturas_canceladas> assinaturas_canceladas { get; set; }
		public DbSet<back_offices> back_offices { get; set; }
		public DbSet<boleto_carnes> boleto_carnes { get; set; }
		public DbSet<canceladas> canceladas { get; set; }
		public DbSet<cartao_de_creditos> cartao_de_creditos { get; set; }
		public DbSet<categoria_material_apoios> categoria_material_apoios { get; set; }
		public DbSet<cidades> cidades { get; set; }
		public DbSet<cidade_parceiros> cidade_parceiros { get; set; }
		public DbSet<clientes> clientes { get; set; }
		public DbSet<clientes_pjs> clientes_pjs { get; set; }
		public DbSet<comissoes_vendas> comissoes_vendas { get; set; }
		public DbSet<consultas> consultas { get; set; }
		public DbSet<contratos> contratos { get; set; }
		public DbSet<debito_em_contas> debito_em_contas { get; set; }
		public DbSet<dependentes> dependentes { get; set; }
		public DbSet<estornos> estornos { get; set; }
		public DbSet<faturas_historicos> faturas_historicos { get; set; }
		public DbSet<fatura_clientes> fatura_clientes { get; set; }
		public DbSet<fatura_supervisores> fatura_supervisores { get; set; }
		public DbSet<fatura_vendedores> fatura_vendedores { get; set; }
		public DbSet<fornecedores> fornecedores { get; set; }
		public DbSet<interessados> interessados { get; set; }
		public DbSet<material_apoios> material_apoios { get; set; }
		public DbSet<migrations> migrations { get; set; }
		public DbSet<parceiros> parceiros { get; set; }
		public DbSet<parceiros_precos> parceiros_precos { get; set; }
		public DbSet<parcerias_corporativas> parcerias_corporativas { get; set; }
		//public DbSet<password_resets> password_resets { get; set; }
		public DbSet<pixes> pixes { get; set; }
		public DbSet<procedimentos> procedimentos { get; set; }
		public DbSet<produtos> produtos { get; set; }
		public DbSet<templates_contratos> templates_contratos { get; set; }
		public DbSet<uploads> uploads { get; set; }
		public DbSet<users> users { get; set; }
		public DbSet<venda_geradas> venda_geradas { get; set; }
		public DbSet<writers> writers { get; set; }


	}
}
