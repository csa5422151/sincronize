﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySqlConnector;

namespace Sincronize_CSA.DATA
{
    public class Conector
    {
        public string cs { private get; set;} 
        public MySqlConnection connection { private get; set;}

        public Conector()
        {
            this.cs = "server=162.241.203.180;user=sempr524_sincronize;database=sempr524_producao;password=S3mPr3@migg0";
            this.connection = new MySqlConnection(this.cs);
        }

        public async Task Update(string query)
        {
            await this.connection.OpenAsync();
            using var command = new MySqlCommand(query, this.connection);
            var reader = await command.ExecuteReaderAsync();
            await this.connection.CloseAsync();   
        }

        public async Task<List<string>> Select(string comand) 
        {           
            await this.connection.OpenAsync();
            using var command = new MySqlCommand(comand, this.connection);
            var reader = await command.ExecuteReaderAsync();
            var listaFinal = new List<string>();

            while (await reader.ReadAsync())
            {
                var value = reader.GetValue(0);
                listaFinal.Add(value.ToString());
            }
            await this.connection.CloseAsync();

            return listaFinal;  
        }

        public List<string> SelectSync(string comand)
        {
            this.connection.Open();
            using var command = new MySqlCommand(comand, this.connection);
            var reader = command.ExecuteReader();
            var listaFinal = new List<string>();

            while (reader.Read())
            {
                var value = reader.GetValue(0);
                listaFinal.Add(value.ToString());
            }
            this.connection.Close();

            return listaFinal;
        }
    }
}
