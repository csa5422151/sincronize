﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Sincronize_CSA.Migrations
{
    public partial class @new : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        { 
            migrationBuilder.AddForeignKey(
                name: "FK_DadosFatura_Refund_refundsid",
                table: "DadosFatura",
                column: "refundsid",
                principalTable: "Refund",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DadosFatura_Refund_refundsid",
                table: "DadosFatura");

            migrationBuilder.DropTable(
                name: "Refund");

            migrationBuilder.DropIndex(
                name: "IX_DadosFatura_refundsid",
                table: "DadosFatura");

            migrationBuilder.DropColumn(
                name: "refundsid",
                table: "DadosFatura");

            migrationBuilder.AddColumn<string>(
                name: "refunds",
                table: "DadosFatura",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");
        }
    }
}
