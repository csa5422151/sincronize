﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Sincronize_CSA.Migrations
{
    public partial class recolocate2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DadosFatura_Refund_refundsid",
                table: "DadosFatura");

            migrationBuilder.DropIndex(
                name: "IX_DadosFatura_refundsid",
                table: "DadosFatura");

            migrationBuilder.DropColumn(
                name: "refundsid",
                table: "DadosFatura");

            migrationBuilder.AlterColumn<double>(
                name: "Value",
                table: "Refund",
                type: "double",
                nullable: true,
                oldClrType: typeof(double),
                oldType: "double");

            migrationBuilder.AlterColumn<string>(
                name: "TransactionReceiptUrl",
                table: "Refund",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "Status",
                table: "Refund",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Refund",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreated",
                table: "Refund",
                type: "datetime(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)");

            migrationBuilder.AddColumn<string>(
                name: "DadosFaturaid",
                table: "Refund",
                type: "varchar(255)",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_Refund_DadosFaturaid",
                table: "Refund",
                column: "DadosFaturaid");

            migrationBuilder.AddForeignKey(
                name: "FK_Refund_DadosFatura_DadosFaturaid",
                table: "Refund",
                column: "DadosFaturaid",
                principalTable: "DadosFatura",
                principalColumn: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Refund_DadosFatura_DadosFaturaid",
                table: "Refund");

            migrationBuilder.DropIndex(
                name: "IX_Refund_DadosFaturaid",
                table: "Refund");

            migrationBuilder.DropColumn(
                name: "DadosFaturaid",
                table: "Refund");

            migrationBuilder.AlterColumn<double>(
                name: "Value",
                table: "Refund",
                type: "double",
                nullable: false,
                defaultValue: 0.0,
                oldClrType: typeof(double),
                oldType: "double",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Refund",
                keyColumn: "TransactionReceiptUrl",
                keyValue: null,
                column: "TransactionReceiptUrl",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "TransactionReceiptUrl",
                table: "Refund",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Refund",
                keyColumn: "Status",
                keyValue: null,
                column: "Status",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "Status",
                table: "Refund",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Refund",
                keyColumn: "Description",
                keyValue: null,
                column: "Description",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Refund",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreated",
                table: "Refund",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "refundsid",
                table: "DadosFatura",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_DadosFatura_refundsid",
                table: "DadosFatura",
                column: "refundsid");

            migrationBuilder.AddForeignKey(
                name: "FK_DadosFatura_Refund_refundsid",
                table: "DadosFatura",
                column: "refundsid",
                principalTable: "Refund",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
