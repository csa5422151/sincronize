﻿//adimplente 1
//inadimplente 0

using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Sincronize_CSA.DATA;
using Sincronize_CSA.http;
using Sincronize_CSA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sincronize_CSA.Robot
{
    public class AtualizaFaturas
    {
        public async Task Run()
        {
            if (VerificaUltimaAtualizacao())
            {                
                var principal = new Principal();
                var client = new Client();
                var db = new ApplicationDbContext();
                var clientesAsaas = db.clientes.Where(x => x.token_asaas != null && !x.token_asaas.Contains("QGX_")).ToList();
                var clientesQGX = db.clientes.Where(x => x.token_asaas != null && x.token_asaas.Contains("QGX_")).ToList();

                var faturasSalvas = db.DadosFatura.ToList();
                //clientes ASAAS

                foreach (var item in clientesAsaas)
                {                    
                    var urlFaturas = "https://www.asaas.com/api/v3/payments?customer=" + item.token_asaas;
                    var resultFaturas = client.Get(urlFaturas);
                    var modelFaturas = JsonConvert.DeserializeObject<Faturas>(resultFaturas);
                    
                    Console.WriteLine("verificando "+ item.cpf);
                    

                    if (faturasSalvas.Where(x => x.customer.Equals(item.token_asaas)).Count() == 0
                        && modelFaturas != null 
                        && modelFaturas.data.Count() > 0
                        ) 
                    {
                        foreach (var i in modelFaturas.data)
                        {
                            if (i.discount == null)
                            {
                                i.discount = new Discount();
                            }

                            if (i.fine == null)
                            { 
                                i.fine = new Fine();    
                            }

                            if (i.interest == null)
                            { 
                                i.interest = new Interest();
                            }

                            if (i.refunds == null)
                            {
                                i.refunds = new List<Refund>();
                            }
                        }
                        Console.WriteLine("atualizando " + item.cpf);

                        db.Faturas.Add(modelFaturas);
                        await db.SaveChangesAsync();

                    }
                }

                //CLIENTES QGX

                foreach (var item in clientesQGX)
                {
                    var listaLocal = new List<DadosFatura>();

                    for(var x = 0; x < 12; x++)
                    {
                        var dataFatura = Convert.ToDateTime(item.created_at);

                        listaLocal.Add(new DadosFatura()
                        {
                            id = "QGX_" + item.cpf + "_" + x,
                            customer = "QGX_" + item.cpf,
                            dateCreated = dataFatura.AddMonths(x).ToString("yyyy-MM-dd"),
                            value = db.produtos.Where(x => x.id.Equals(item.id_produto)).FirstOrDefault().valor
                        }) ;
                    }

                    var modelFaturas = new Faturas() { data = listaLocal};

                    Console.WriteLine("verificando " + item.cpf);

                    if (faturasSalvas.Where(x => x.customer.Equals(item.token_asaas)).Count() == 0
                        && modelFaturas != null
                        && modelFaturas.data.Count() > 0
                        )
                    {
                        foreach (var i in modelFaturas.data)
                        {
                            if (i.discount == null)
                            {
                                i.discount = new Discount();
                            }

                            if (i.fine == null)
                            {
                                i.fine = new Fine();
                            }

                            if (i.interest == null)
                            {
                                i.interest = new Interest();
                            }

                            if (i.refunds == null)
                            {
                                i.refunds = new List<Refund>();
                            }
                        }
                        Console.WriteLine("atualizando " + item.cpf);

                        db.Faturas.Add(modelFaturas);
                        await db.SaveChangesAsync();

                    }
                }


                var atualizaHorario = "update robos set ultima='" + DateTime.Now.AddHours(2) + "' where funcionalidade = '" + (int)EFuncionalidades.AtualizaFaturas + "'";
                await principal.update(atualizaHorario);
                Console.WriteLine("Horario de atualização atualizado!");
            }
        }
        public async Task AtualizarStatusFaturas()
        {
            if (VerificaUltimaAtualizacaoStatus())
            {
                var principal = new Principal();
                var client = new Client();
                var db = new ApplicationDbContext();
                var faturasSalvas = db.DadosFatura.Where(x => !x.customer.Contains("QGX_")).ToList();                

                foreach (var item in faturasSalvas)
                {
                    var urlFaturas = "https://www.asaas.com/api/v3/payments/" + item.id;
                    var resultFaturas = client.Get(urlFaturas);
                    var modelFaturas = JsonConvert.DeserializeObject<DadosFatura>(resultFaturas);                    

                    Console.WriteLine("verificando " + item.id);                    
                                        
                    if (modelFaturas != null)
                    {
                        Console.WriteLine("atualizando " + item.id);
                        db.Entry(item).CurrentValues.SetValues(modelFaturas);

                        await db.SaveChangesAsync();
                    }
                }

                var atualizaHorario = "update robos set ultima='" + DateTime.Now.AddHours(2) + "' where funcionalidade = '" + (int)EFuncionalidades.AtualizaStatusFaturas + "'";
                await principal.update(atualizaHorario);
                Console.WriteLine("Horario de atualização atualizado!");
            }
        }
        public async Task AtualizaAdimplentes()
        {
            if (VerificaUltimaAtualizacaoAdimplentes())
            {
                var principal = new Principal();
                var db = new ApplicationDbContext();
                var clientesAsaas = db.clientes.Where(x => x.token_asaas != null && !x.token_asaas.Contains("QGX_")).ToList();
                var faturasSalvas = db.DadosFatura.Where(x => !x.customer.Contains("QGX_")).ToList();

                foreach (var item in clientesAsaas)
                {
                    Console.WriteLine("verificando " + item.cpf);                    

                    if (faturasSalvas.Where(x => x.customer.Equals(item.token_asaas)).Count() > 0)
                    {                        
                        var listaFaturas = faturasSalvas.Where(x => x.customer.Equals(item.token_asaas)).ToList();
                        if (listaFaturas.Any(x=> x.status.Equals("OVERDUE")))
                        {
                            Console.WriteLine("atualizando " + item.cpf);
                            var EditedObj = clientesAsaas.Where(x => x.token_asaas.Equals(item.token_asaas)).ToList().FirstOrDefault();
                            EditedObj.adimplente = 0;

                            db.Entry(EditedObj).CurrentValues.SetValues(EditedObj);
                            await db.SaveChangesAsync();
                        }
                    }
                }

                var atualizaHorario = "update robos set ultima='" + DateTime.Now.AddHours(2) + "' where funcionalidade = '" + (int)EFuncionalidades.AtualizaAdimplentesFaturas + "'";
                await principal.update(atualizaHorario);
                Console.WriteLine("Horario de atualização atualizado!");
            }
        }
        public bool VerificaUltimaAtualizacao()
        {
            var principal = new Principal();
            var buscaCpf = "select ultima from robos where funcionalidade = '" + (int) EFuncionalidades.AtualizaFaturas + "'";
            var item = principal.SelectSync(buscaCpf);
            var dataUltima = Convert.ToDateTime(item.FirstOrDefault().ToString());

            var comparar = dataUltima < DateTime.Now;

            if (comparar)
            {
                Console.WriteLine("AtualizaFaturas: RODANDO");
                return true;
            }

            Console.WriteLine("AtualizaFaturas: AGUARDANDO PROXIMA RODADA");
            return false;
        }
        public bool VerificaUltimaAtualizacaoStatus()
        {
            var principal = new Principal();
            var buscaCpf = "select ultima from robos where funcionalidade = '" + (int)EFuncionalidades.AtualizaStatusFaturas + "'";
            var item = principal.SelectSync(buscaCpf);
            var dataUltima = Convert.ToDateTime(item.FirstOrDefault().ToString());

            var comparar = dataUltima < DateTime.Now;
            var compararst = dataUltima.Hour;

            if (comparar)
            {
                Console.WriteLine("AtualizaStatusFaturas: RODANDO");
                return true;
            }

            Console.WriteLine("AtualizaStatusFaturas: AGUARDANDO PROXIMA RODADA");
            return false;
        }
        public bool VerificaUltimaAtualizacaoAdimplentes()
        {
            var principal = new Principal();
            var buscaCpf = "select ultima from robos where funcionalidade = '" + (int)EFuncionalidades.AtualizaAdimplentesFaturas + "'";
            var item = principal.SelectSync(buscaCpf);
            var dataUltima = Convert.ToDateTime(item.FirstOrDefault().ToString());

            var comparar = dataUltima < DateTime.Now;
            var compararst = dataUltima.Hour;

            if (comparar)
            {
                Console.WriteLine("AtualizaAdimplentesFaturas: RODANDO");
                return true;
            }

            Console.WriteLine("AtualizaAdimplentesFaturas: AGUARDANDO PROXIMA RODADA");
            return false;
        }
    }
}
