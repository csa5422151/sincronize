﻿using Sincronize_CSA.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySqlConnector;

namespace Sincronize_CSA.Robot
{
    public class Principal
    {
        public async Task<List<string>> Select(string query)
        {
            var conector = new Conector();
            return await conector.Select(query);
           
        }

        public List<string> SelectSync(string query)
        {
            var conector = new Conector();
            return conector.SelectSync(query);
        }

        public async Task update(string query)
        {
            var conector = new Conector();
            await conector.Update(query);
        }
    }
}
