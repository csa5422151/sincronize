﻿using Newtonsoft.Json;
using Sincronize_CSA.http;
using Sincronize_CSA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sincronize_CSA.Robot
{
    public class AtualizaTokenQGX
    {
        public async Task Run()
        {
            if (VerificaUltimaAtualizacao())
            {
                var principal = new Principal();
                var buscaCpf = "select cpf from clientes where token_asaas is null";
                var lista = await principal.Select(buscaCpf);

                foreach (var item in lista)
                {
                    Console.WriteLine("verifica" + item);
                    item.Replace(".", "");
                    item.Replace("-", "");

                    var url = "https://baas-sandbox.qgx.codes/api/v1/dashboard/accounts";

                    var client = new Client_qgx();

                    var result = client.Get(url);

                    if (result != string.Empty)
                    {
                        var model = JsonConvert.DeserializeObject<Clientes>(result);

                        if (model.data.Count > 0)
                        {
                            var custumer_id = model.data.FirstOrDefault().id;

                            Console.WriteLine("atualizar "+ item);

                            var atualizaToken = @"update clientes set token_asaas='" + custumer_id + "' where cpf ='" + item + "'";
                            await principal.update(atualizaToken);
                            Console.WriteLine(custumer_id + " atualizado!");                           
                        }
                    }
                }

                var atualizaHorario = "update robos set ultima='"+ DateTime.Now.AddHours(2) + "' where funcionalidade = '" + (int) EFuncionalidades.AtualizaTokenAsaas + "'";
                await principal.update(atualizaHorario);
                Console.WriteLine("Horario de atualização atualizado!");
            }
        }

        public bool VerificaUltimaAtualizacao()
        {
            var principal = new Principal();
            var buscaCpf = "select ultima from robos where funcionalidade = '"+((int) EFuncionalidades.AtualizaTokenAsaas)+"'";
            var item = principal.SelectSync(buscaCpf);
            var dataUltima = Convert.ToDateTime(item.FirstOrDefault().ToString());

            var comparar = dataUltima < DateTime.Now;

            if (comparar)
            {
                Console.WriteLine("atualizarToken: RODANDO");
                return true;               
            }

            Console.WriteLine("atualizarToken: AGUARDANDO PROXIMA RODADA");
            return false;
        }
    }
}
