﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace Sincronize_CSA.http
{
    public class Client
    {
        public string ApiClient { get; set; }
        public string Access_token { get; set; }
        public string Content_Type { get; set; }

        public Client()
        {
            this.Access_token = "b65d4c667849758231074b9bffa835f9236ca339a3cf173744c58ce98bc97273";
            this.Content_Type = "application/json";
        }

        public string Get(string endpoint) 
        {
            using var httpClient = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, endpoint);
            request.Headers.Add("access_token", this.Access_token);
            //request.Headers.Add("Content-Type", this.Content_Type);
            var Content = new StringContent("json", Encoding.UTF8, "application/json");
            request.Content = Content;  

            var response = httpClient.Send(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                using var reader = new StreamReader(response.Content.ReadAsStream());
                return reader.ReadToEnd();
            }
            
            return string.Empty;
        }

        public void Post()
        {
            /*var handler = new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };

            var client = new HttpClient(handler)
            {
                BaseAddress = new Uri(_baseurl)
            };

            var webRequest = new HttpRequestMessage(HttpMethod.Post, "sms")
            {
                Content = new StringContent(json, Encoding.UTF8, "application/json")
            };

            webRequest.Headers.Add("user_key", tokens[0]);
            webRequest.Headers.Add("Session_key", tokens[1]);

            var response = client.Send(webRequest);
            var reader = new StreamReader(response.Content.ReadAsStream());
            var responseBody = reader.ReadToEnd();*/

        }
    }
}
